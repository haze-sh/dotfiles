import os

username = os.environ['HYPOTHESIS_USER']
token = os.environ['HYPOTHESIS_PASS']
